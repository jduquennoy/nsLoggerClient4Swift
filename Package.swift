import PackageDescription

let package = Package(
  name: "NSLoggerClient",
  targets: [
    Target(name: "NSLogger"),
    Target(name: "Demo", dependencies: [.Target(name: "NSLogger")])
  ]
)
