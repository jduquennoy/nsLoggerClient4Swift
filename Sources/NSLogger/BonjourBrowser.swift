//
//  BonjourBrowser.swift
//  NSLoggerClient
//
//  Created by Jérôme Duquennoy on 20/07/16.
//
//

import Foundation

/**
 Use this protocol to receive hosts found by the network service browser.
 */
protocol HostBrowserDelegate {
  func remoteHostDiscovered(_ hostname: String, port: UInt16)
}

#if os(iOS) || os(OSX) || os(watchOS)

/**
 This class will browse for domains and services on the network matching a given name, resolve found services,
 and report the host name and port to connect to a delegate.
 */
final class BonjourBrowser: NSObject {
  fileprivate static let localDomain = "local."
  
  fileprivate let serviceTypeToSearch: String
  fileprivate let serviceNameToSearch: String?
  fileprivate let domainsBrowser = NetServiceBrowser()
  fileprivate var servicesBrowsers = [NetServiceBrowser]()
  fileprivate var resolvingServices = [NetService]()
  
  internal var delegate: HostBrowserDelegate? = nil
  
  init(serviceType: String, serviceName: String? = nil, limitToLocalDomain: Bool, delegate: HostBrowserDelegate) {
    self.serviceTypeToSearch = serviceType
    self.delegate = delegate
    self.serviceNameToSearch = serviceName
    
    super.init()
    
    self.domainsBrowser.delegate = self
    
    if limitToLocalDomain {
      // simulate the discovery of the local domain
      self.netServiceBrowser(self.domainsBrowser, didFindDomain: BonjourBrowser.localDomain, moreComing: false)
    } else {
      self.domainsBrowser.searchForBrowsableDomains()
    }
  }
  
  deinit {
    self.domainsBrowser.stop()
    self.servicesBrowsers.forEach { $0.stop() }
    self.servicesBrowsers.removeAll()
    
    self.resolvingServices.forEach { $0.stop() }
    self.servicesBrowsers.removeAll()
    
    self.delegate = nil
  }
}

extension BonjourBrowser: NetServiceBrowserDelegate {
  func netServiceBrowser(_ browser: NetServiceBrowser, didFindDomain domainString: String, moreComing: Bool) {
    let servicesBrowser = NetServiceBrowser()
    servicesBrowser.delegate = self
    servicesBrowser.searchForServices(ofType: self.serviceTypeToSearch, inDomain: domainString)
    
    servicesBrowsers.append(servicesBrowser)
  }
  
  func netServiceBrowser(_ browser: NetServiceBrowser, didFind service: NetService, moreComing: Bool) {
    guard(self.serviceNameToSearch == nil || service.name == self.serviceNameToSearch) else {
      return
    }
    
    service.delegate = self
    service.resolve(withTimeout: 5.0)
    self.resolvingServices.append(service)
  }
  
  func netServiceBrowser(_ browser: NetServiceBrowser, didNotSearch errorDict: [String : NSNumber]) {
    
    if browser === self.domainsBrowser {
      NSLog("Domains browser failed to search : \(errorDict)")
    } else {
      NSLog("Browser failed to search : \(errorDict)")
      if let browserIndex = self.servicesBrowsers.index(of: browser) {
        self.servicesBrowsers.remove(at: browserIndex)
      }
    }
  }
}

extension BonjourBrowser: NetServiceDelegate {  
  func netService(_ sender: NetService, didNotResolve errorDict: [String : NSNumber]) {
    NSLog("Service failed to resolve: \(errorDict)")
  }
  
  func netServiceDidResolveAddress(_ service: NetService) {
    service.delegate = nil
    if let hostname = service.hostName {
      self.delegate?.remoteHostDiscovered(hostname, port: UInt16(service.port))
    }
    if let serviceIndex = self.resolvingServices.index(of: service) {
      self.resolvingServices.remove(at: serviceIndex)
    }
  }
}

#endif
