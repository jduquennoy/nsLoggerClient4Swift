//
//  NSLoggerClient.swift
//  nsloggerClient
//
//  Created by Jérôme Duquennoy on 01/10/2015.
//  Copyright © 2015 duquennoy. All rights reserved.
//

import Foundation

internal enum MessagePartKind: UInt8 {
  case type = 0
  case timestampSeconds = 1
  case timestampMilliseconds = 2
  case timestampMicroseconds = 3
  case threadId = 4
  case tag = 5
  case level = 6
  case message = 7
  case imageWidth = 8
  case imageHeight = 9
  case sequenceNumber = 10
  case fileName = 11
  case lineNumber = 12
  case functionName = 13
  case clientName = 20
  case clientVersion = 21
  case osName = 22
  case osVersion = 23
  case clientHardware = 24
  case uniqueId = 25
}

internal enum MessagePartPayload {
  case string(String)
  case binary(Data)
  case uint16(UInt16)
  case uint32(UInt32)
  case uint64(UInt64)
  case image(Data) // image in PNG format
  
  func typeCode() -> UInt8 {
    switch(self) {
    case .string: return 0
    case .binary: return 1
    case .uint16: return 2
    case .uint32: return 3
    case .uint64: return 4
    case .image: return 5
    }
  }
}

internal struct MessagePart {
  let kind: MessagePartKind
  let payload: MessagePartPayload
}

public final class NSLoggerClient: NSObject, StreamDelegate {
  internal static var disableSSL: Bool = false // used for testing purpose only
  
  public enum Level: UInt32 {
    case error = 0
    case warning = 1
    case info = 2
    case debug = 3
    case trace = 4
  }
  
  internal enum MessagePartType: UInt8 {
    case string = 0 // UTF8 encoded string
    case binary = 1
    case int16 = 2
    case int32 = 3
    case int64 = 4
    case image = 5 // image in PNG format
  }
  
  internal enum MessageType: UInt32 {
    case log = 0
    case blockStart = 1
    case blockEnd = 2
    case clientInfo = 3
  }

  // configuration & status variables
  let useLocalCache: Bool
  var messagesSequenceNumber: UInt32 = 1
  var isIdle = false
  var isConnected: Bool {
    return self.writeStream != nil
  }

  // network related variables
  var remoteHost: (hostname: String, port: UInt16)? = nil
  var writeStream: OutputStream? = nil
  
  #if os(OSX) || os(iOS) || os(watchOS) || os(tvOS)
  var bonjourBrowser: BonjourBrowser? = nil
  #endif
  
  // mutex
  fileprivate let messageQueueMutex = Mutex()
  fileprivate let writeStreamMutex = Mutex()
  
  fileprivate var messagesQueue = Array<Data>() 
  
  public init(host: String = "localhost", port: UInt16 = 50000, useLocalCache: Bool = true) {
    
    self.remoteHost = (hostname: host, port: port)
    self.useLocalCache = useLocalCache
    
    super.init()

    self.openNetworkConnection(true)
  }
  
  #if os(OSX) || os(iOS) || os(watchOS) || os(tvOS)
  public init(bonjourServiceName: String? = nil, useLocalCache: Bool = true, limitToLocalDomain: Bool = true) {
    self.useLocalCache = useLocalCache
    
    super.init()

    self.bonjourBrowser = BonjourBrowser(serviceType: "_nslogger-ssl._tcp", serviceName: bonjourServiceName, limitToLocalDomain: limitToLocalDomain, delegate: self)
  }
  #endif
  
  deinit {
    self.closeNetworkConnection()

    #if os(OSX) || os(iOS) || os(watchOS) || os(tvOS)
    self.bonjourBrowser = nil
    #endif
  }
  
  /// Queues a text message to be sent.
  /// The message might be delivered imeditely if a server is available,
  /// delayed if not but local cache is enabled
  /// or dropped if no server is connected and local cache is disabled
  public func queue(textMessage text: String, domain: String, level: Level, timestamp: TimeInterval, threadName: String, fileName: String?, lineNumber: UInt32?, functionName: String?) {
    let messageData = self.createMessageData(timestamp, threadName: threadName)
    
    messageData.addLoggerMessagePart(.type, payload: .uint32(MessageType.log.rawValue))
    messageData.addLoggerMessagePart(.tag, payload: .string(domain))
    messageData.addLoggerMessagePart(.level, payload: .uint32(level.rawValue))
    messageData.addLoggerMessagePart(.message, payload: .string(text))
    
    if let fileName = fileName {
      messageData.addLoggerMessagePart(.fileName, payload: .string(fileName))
    }
    if let lineNumber = lineNumber {
      messageData.addLoggerMessagePart(.lineNumber, payload: .uint32(lineNumber))
    }
    if let functionName = functionName {
      messageData.addLoggerMessagePart(.functionName, payload: .string(functionName))
    }
    
    self.queueMessage(messageData)
  }
  
  /// Queues an image to be sent. The image must be provided as GIF data.
  /// The image might be delivered imeditely if a server is available,
  /// delayed if not but local cache is enabled
  /// or dropped if no server is connected and local cache is disabled
  public func queue(gifImage imageData: Data, domain: String, level: Level, timestamp: TimeInterval, threadName: String, fileName: String?, lineNumber: UInt32?, functionName: String?) {
    let messageData = self.createMessageData(timestamp, threadName: threadName)
    
    messageData.addLoggerMessagePart(.type, payload: .uint32(MessageType.log.rawValue))
    messageData.addLoggerMessagePart(.tag, payload: .string(domain))
    messageData.addLoggerMessagePart(.level, payload: .uint32(level.rawValue))
    messageData.addLoggerMessagePart(.message, payload: .image(imageData))
    
    if let fileName = fileName {
      messageData.addLoggerMessagePart(.fileName, payload: .string(fileName))
    }
    if let lineNumber = lineNumber {
      messageData.addLoggerMessagePart(.lineNumber, payload: .uint32(lineNumber))
    }
    if let functionName = functionName {
      messageData.addLoggerMessagePart(.functionName, payload: .string(functionName))
    }
    
    self.queueMessage(messageData)
  }
  
  /// Queues raw binary data to be sent.
  /// The data might be delivered imeditely if a server is available,
  /// delayed if not but local cache is enabled
  /// or dropped if no server is connected and local cache is disabled
  public func queue(binaryMessage data: Data, domain: String, level: Level, timestamp: TimeInterval, threadName: String, fileName: String?, lineNumber: UInt32?, functionName: String?) {
    let messageData = self.createMessageData(timestamp, threadName: threadName)
    
    messageData.addLoggerMessagePart(.type, payload: .uint32(MessageType.log.rawValue))
    messageData.addLoggerMessagePart(.tag, payload: .string(domain))
    messageData.addLoggerMessagePart(.level, payload: .uint32(level.rawValue))
    messageData.addLoggerMessagePart(.message, payload: .binary(data))
    
    if let fileName = fileName {
      messageData.addLoggerMessagePart(.fileName, payload: .string(fileName))
    }
    if let lineNumber = lineNumber {
      messageData.addLoggerMessagePart(.lineNumber, payload: .uint32(lineNumber))
    }
    if let functionName = functionName {
      messageData.addLoggerMessagePart(.functionName, payload: .string(functionName))
    }
    
    self.queueMessage(messageData)
  }

  fileprivate func setupMemoryAlertHandling() {
    let source = DispatchSource.makeMemoryPressureSource(eventMask: [.warning, .critical], queue: DispatchQueue.main)
    source.setEventHandler { 
      NSLog("Memory warning received, clearing local cache")
      self.messagesQueue.removeAll();
    }
    source.resume()
  }
  
  fileprivate func openNetworkConnection(_ retryIfConnectionFailed: Bool) {
    NSLog("Trying to open connection")
    
    if let remoteHost = self.remoteHost {
      self.openTcpStream(remoteHost.hostname, port: remoteHost.port)
    }
    
    if retryIfConnectionFailed && !self.isConnected {
      self.scheduleNextConnectionTry()
    }
  }
  
  fileprivate func closeNetworkConnection() {
    guard self.isConnected else { return }

    _ = self.writeStreamMutex.executeSafe {
      self.writeStream?.remove(from: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
      self.writeStream?.close()
      self.writeStream = nil
      NSLog("stream closed")
    }
  }
  
  fileprivate func scheduleNextConnectionTry()
  {
    let nextConnectionTryTime = DispatchTime.now() + 1.0
    
    let connectionClosure:@convention(block) () -> Void = {[weak self] in
      if let weakSelf = self {
        weakSelf.openNetworkConnection(true)
      }
    }
    
    if #available(OSXApplicationExtension 10.10, *) {
      DispatchQueue.main.asyncAfter(deadline: nextConnectionTryTime, qos: DispatchQoS.background, execute: connectionClosure)
    } else {
      DispatchQueue.main.asyncAfter(deadline: nextConnectionTryTime, execute: connectionClosure)
    }
  }
  
  fileprivate func openTcpStream(_ host: String, port: UInt16) {
    if self.isConnected {
      self.closeNetworkConnection()
    }
    
    _ = self.writeStreamMutex.executeSafe {
      var writeStream: Unmanaged<CFWriteStream>?
      CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, host as CFString, UInt32(port), nil, &writeStream)

      self.writeStream = writeStream?.takeRetainedValue()
      
      if let writeStream = self.writeStream {
        if !NSLoggerClient.disableSSL {
          let sslSettings:[String: AnyObject] = [kCFStreamSSLLevel as String: kCFStreamSocketSecurityLevelNegotiatedSSL,
                                                 kCFStreamSSLValidatesCertificateChain as String: false as AnyObject,
                                                 kCFStreamSSLIsServer as String: false as AnyObject,
                                                 kCFStreamSSLPeerName as String: kCFNull]
          let sslSettingsKeyString: String = kCFStreamPropertySSLSettings as String
        
          writeStream.setProperty(sslSettings, forKey: Stream.PropertyKey(sslSettingsKeyString))
        }
        
        writeStream.delegate = self
        
        writeStream.schedule(in: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
        writeStream.open()
        // todo release stream mutex
        
        NSLog("tcp stream setup completed")
      }
    }
  }
  
  public func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
    switch eventCode {
    case Stream.Event.openCompleted:
      self.queueClientInfo()
      NSLog("tcp stream opened")
    case Stream.Event.hasSpaceAvailable:
      NSLog("tcp stream can accept data")
      self.sendNextMessageOnQueue()
    case Stream.Event.endEncountered:
      NSLog("tcp stream did end")
      self.closeNetworkConnection()
      #if os(OSX) || os(iOS) || os(watchOS) || os(tvOS)
      if(self.bonjourBrowser == nil) {
        self.scheduleNextConnectionTry()
      }
      #endif
    case Stream.Event.errorOccurred:
      NSLog("tcp stream error: \(eventCode)")
      self.closeNetworkConnection()
      self.scheduleNextConnectionTry()
    default:
      break;
    }
  }
  
  fileprivate func queueClientInfo() {
    let timestamp = Date().timeIntervalSince1970
    let threadName = "Thread 3"
    let messageData = self.createMessageData(timestamp, threadName: threadName, includeSequenceNumber: false)
    
    let clientInfo = self.getClientInfos()
    let clientName = clientInfo.name
    let clientVersion = clientInfo.version
    
    let hostInfo = self.getHostInfos()
    let osName = hostInfo.osName
    let osVersion = hostInfo.osVersion
    let hardwareModel = hostInfo.hardwarePlatform
    
    messageData.addLoggerMessagePart(.type, payload: .uint32(MessageType.clientInfo.rawValue))
    messageData.addLoggerMessagePart(.clientVersion, payload: .string(clientVersion))
    messageData.addLoggerMessagePart(.clientName, payload: .string(clientName))
    messageData.addLoggerMessagePart(.osVersion, payload: .string(osVersion))
    messageData.addLoggerMessagePart(.osName, payload: .string(osName))
    messageData.addLoggerMessagePart(.clientHardware, payload: .string(hardwareModel))
    
    self.queueMessage(messageData, first: true)
  }
  
  fileprivate func getClientInfos() -> (name: String, version: String) {
    let name: String
    let version: String
    
    if let bundleInfo = Bundle.main.infoDictionary {
      name = bundleInfo[kCFBundleNameKey as String] as? String ?? "-"
      version = bundleInfo[kCFBundleVersionKey as String] as? String ?? "-"
    } else {
      name = "-"
      version = "-"
    }

    return (name, version)
  }
  
  fileprivate func createMessageData(_ timestamp: TimeInterval, threadName: String, includeSequenceNumber: Bool = true) -> NSMutableData {
    let messageData = NSMutableData()
    
    // insert a placeholder for data size, that will be adjusted before sending the data
    var dataSize = UInt32(0).bigEndian
    messageData.append(&dataSize, length: MemoryLayout<UInt32>.size)
    
    // insert placeholder for parts count, that will be incremented each time we add a part
    var partsCount = UInt16(0).bigEndian
    messageData.append(&partsCount, length: MemoryLayout<UInt16>.size)
    
    if includeSequenceNumber {
      messageData.addLoggerMessagePart(.sequenceNumber, payload: .uint32(self.messagesSequenceNumber))
      self.messagesSequenceNumber += 1
    }
    
    let timestampSeconds = UInt64(timestamp)
    let timestampMicroseconds = UInt64((timestamp - Double(timestampSeconds)) * 1000000)
    
    messageData.addLoggerMessagePart(.timestampSeconds, payload: .uint64(timestampSeconds))
    messageData.addLoggerMessagePart(.timestampMicroseconds, payload: .uint64(timestampMicroseconds))
    messageData.addLoggerMessagePart(.threadId, payload: .string(threadName))

    return messageData
  }
  
  fileprivate func queueMessage(_ messageData: NSMutableData, first: Bool = false)
  {
    // The size to write in the first 4 bits of data should not take itself into account
    var dataSize = UInt32(messageData.length - 4).bigEndian
    messageData.replaceBytes(in: NSRange(location: 0, length: MemoryLayout<UInt32>.size), withBytes: &dataSize)

    NSLog("queuing message of size \(messageData.length)")
    
    _ = self.messageQueueMutex.executeSafe {
      if(!self.useLocalCache) {
        self.messagesQueue.removeAll()
      }
      if first {
        self.messagesQueue.insert(messageData as Data, at: 0)
      } else {
        self.messagesQueue.append(messageData as Data)
      }
    }      
    if self.isIdle {
      self.sendNextMessageOnQueue()
    }
  }
  
  fileprivate func sendNextMessageOnQueue() {
    guard let writeStream = self.writeStream else { return }
    guard writeStream.hasSpaceAvailable else { return }

    guard self.messageQueueMutex.lock() == 0 else {
      NSLog("Failed to lock mutex to send messages");
      return;
    }
    defer {
      _ = self.messageQueueMutex.unlock()
    }
    
    guard self.writeStreamMutex.lock() == 0 else {
      NSLog("Failed to lock mutex to send messages");
      return;
    }
    defer {
      _ = self.writeStreamMutex.unlock()
    }
    
    guard !self.messagesQueue.isEmpty else {
      NSLog("going to idle")
      self.isIdle = true
      return
    }
    self.isIdle = false
    
    let messageData = self.messagesQueue.removeFirst()

    NSLog("sending message of size \(messageData.count)")
    let dataToWrite = (messageData as NSData).bytes.assumingMemoryBound(to: UInt8.self)
    var writtenData = writeStream.write(dataToWrite, maxLength: messageData.count)
    writtenData = max(0, writtenData) // written data can be -1 in case of error
    if writtenData < messageData.count {
      let remainingData = messageData.subdata(in: Range(uncheckedBounds: (lower: writtenData, upper: messageData.count)))
      self.messagesQueue.insert(remainingData, at: 0)
    }
    
    NSLog("   \(writtenData) bytes sent")
  }
}

extension NSLoggerClient : HostBrowserDelegate {
  func remoteHostDiscovered(_ hostname: String, port: UInt16) {
    self.closeNetworkConnection()
    self.remoteHost = (hostname: hostname, port: port)
    self.openNetworkConnection(false)
  }
}

#if os(OSX)
  //MARK:- OS X specifics
  extension NSLoggerClient {

    fileprivate func getHostInfos() -> (osName: String, osVersion: String, hardwarePlatform: String, deviceName: String) {
      var result: (osName: String, osVersion: String, hardwarePlatform: String, deviceName: String)
      
      result.osName = "MacOS"
      result.osVersion = ProcessInfo().operatingSystemVersionString
      result.hardwarePlatform = "TODO"
      result.deviceName = "TODO"
      
      return result
    }
  }
#elseif os(iOS) || os(watchOS) || os(tvOS)
  //MARK:- iOS / watchOS / tvOS specifics
  import UIKit

  extension NSLoggerClient {
    
    private func getHostInfos() -> (osName: String, osVersion: String, hardwarePlatform: String, deviceName: String) {
      var result: (osName: String, osVersion: String, hardwarePlatform: String, deviceName: String)
      
      let device = UIDevice.current()
      result.osName = device.systemName
      result.osVersion = device.systemVersion
      result.hardwarePlatform = device.model
      result.deviceName = device.name
      
      return result    
    }
  }

#else
  //MARK:- Unknown OS specifics
  extension NSLoggerClient {
    private func getHostInfos() -> (osName: String, osVersion: String, hardwarePlatform: String, deviceName: String) {
      return (osName: "Unknown", osVersion: "Unknown", hardwarePlatform: "Unknown", deviceName: "Unknown")
    }
  }
  
  //    var systemInfo = utsname()
  //    let returnCode = uname(&systemInfo)
  //    if returnCode == 0 {
  //      ProcessInfo().
  //      result.hardwarePlatform = NSString(bytes: &systemInfo.machine, length: Int(_SYS_NAMELEN), encoding: String.Encoding.ascii.rawValue) as? String ?? "-"
  //      result.osName = NSString(bytes: &systemInfo.sysname, length: Int(_SYS_NAMELEN), encoding: String.Encoding.ascii.rawValue) as? String ?? "-"
  //      result.osVersion = NSString(bytes: &systemInfo.version, length: Int(_SYS_NAMELEN), encoding: String.Encoding.ascii.rawValue) as? String ?? "-"
  //    } else {
  //      result.hardwarePlatform = "-"
  //      result.osName = "-"
  //      result.osVersion = "-"
  //    }
  
  
#endif

