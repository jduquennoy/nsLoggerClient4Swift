//
//  LinuxUtilities.swift
//  NSLoggerClient
//
//  Created by Jérôme Duquennoy on 29/07/16.
//
//

#if os(Linux)

import Foundation
  
func NSLog(format: String) {
  print(format)
  
}
  
#endif
