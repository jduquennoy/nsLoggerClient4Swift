//
//  NSData+loggerClient.swift
//  NSLoggerClient
//
//  Created by Jérôme Duquennoy on 25/07/16.
//
//

import Foundation

internal extension NSMutableData {
  
  func addLoggerMessagePart(_ kind: MessagePartKind, payload: MessagePartPayload) {
    self.incrementPartsCount()
    
    var partKindValue = kind.rawValue
    self.append(&partKindValue, length: MemoryLayout<UInt8>.size)
    
    var valueType = payload.typeCode()
    self.append(&valueType, length: MemoryLayout<UInt8>.size)
    
    switch(payload) {
    case .uint16(let value):
      var mutableValue = value.bigEndian
      self.append(&mutableValue, length: MemoryLayout<(UInt16)>.size)
    case .uint32(let value):
      var mutableValue = value.bigEndian
      self.append(&mutableValue, length: MemoryLayout<(UInt32)>.size)
    case .uint64(let value):
      var mutableValue = value.bigEndian
      self.append(&mutableValue, length: MemoryLayout<(UInt64)>.size)
    case .string(let string):
      let textData = string.data(using: String.Encoding.utf8, allowLossyConversion: true) ?? Data()
      self.addLoggerData(textData)
    case .binary(let data):
      self.addLoggerData(data)
    case .image(let data):
      self.addLoggerData(data)
    }
  }
  
  fileprivate final func addLoggerData(_ data: Data) {
    let dataLengthInBytes = data.count
    var dataLengthToSend = UInt32(dataLengthInBytes).bigEndian
    self.append(&dataLengthToSend, length: MemoryLayout<UInt32>.size)
    self.append(data as Data)
  }
  
  fileprivate final func incrementPartsCount() {
    guard self.length >= 6 else { return }
    
    let partsCountRange = NSRange(location: 4, length: 2)
    
    var currentPartsCount: UInt16 = 0
    self.getBytes(&currentPartsCount, range: partsCountRange)
    
    currentPartsCount = (currentPartsCount.bigEndian + 1).bigEndian
    self.replaceBytes(in: partsCountRange, withBytes: &currentPartsCount)
  }
  
}
