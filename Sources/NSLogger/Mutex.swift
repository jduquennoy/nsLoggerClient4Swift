//
//  Mutex.swift
//  NSLoggerClient
//
//  Created by Jérôme Duquennoy on 20/07/16.
//
//

import Foundation

/***
 * A very simple mutex class that wraps a pthread mutex.
 */
final class Mutex {
  fileprivate let mutex = UnsafeMutablePointer<pthread_mutex_t>.allocate(capacity: 1)
  
  init() {
    pthread_mutex_init(mutex, nil)
  }
  
  func lock() -> Int32 {
    return pthread_mutex_lock(mutex)
  }
  
  func unlock() -> Int32 {
    return pthread_mutex_unlock(mutex)
  }
  
  func tryLock() -> Int32 {
    return pthread_mutex_trylock(mutex)
  }
  
  deinit {
    pthread_mutex_destroy(mutex)
    mutex.deinitialize()
  }
  
  func executeSafe( _ closure: () -> () ) -> Bool {
    if(self.lock() == 0) {
      closure()
      _ = self.unlock()
      return true
    } else {
      return false
    }
  }
}

