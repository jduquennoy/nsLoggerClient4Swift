//
//  main.swift
//  nsloggerClient
//
//  Created by Jérôme Duquennoy on 01/10/2015.
//  Copyright © 2015 duquennoy. All rights reserved.
//

import Foundation
import Cocoa
import NSLogger

//let client = NSLoggerClient(host: "127.0.0.1", port: 4233, useLocalCache: false)
let client = NSLoggerClient(bonjourServiceName: "test", useLocalCache: true, limitToLocalDomain: true)

RunLoop.main.run(until: Date(timeIntervalSinceNow: 1.0))
client.queue(textMessage: "testLog", domain: "category", level: .info, timestamp: 1465473479.0531681, threadName: "Main thread", fileName: "test", lineNumber: 42, functionName: "testfn")

RunLoop.main.run(until: Date(timeIntervalSinceNow: 1.0))

let image = NSImage(named: NSImageNameApplicationIcon)!
let gifImageData = NSBitmapImageRep(cgImage: image.cgImage(forProposedRect: nil, context: nil, hints: nil)!).representation(using: .GIF, properties: [:])
client.queue(gifImage: gifImageData!, domain: "category", level: .info, timestamp: Date().timeIntervalSince1970, threadName: "test", fileName: #file, lineNumber: #line, functionName: #function)
client.queue(binaryMessage: gifImageData!, domain: "category", level: .info, timestamp: Date().timeIntervalSince1970, threadName: "test", fileName: #file, lineNumber: #line, functionName: #function)
for index in 1...60 {
  client.queue(textMessage: "testLog 2.\(index)", domain: "category", level: .error, timestamp: 1465473479.0531681, threadName: "Main thread", fileName: "test", lineNumber: 42, functionName: "testfn")

  RunLoop.main.run(until: Date(timeIntervalSinceNow: 1.0))
}

extension Data {
  
  var hexString: String? {
    let charA = UInt8(UnicodeScalar("a").value)
    let char0 = UInt8(UnicodeScalar("0").value)

    func itoh(_ value: UInt8) -> UInt8 {
      return (value > 9) ? (charA + value - 10) : (char0 + value)
    }
    
    return self.withUnsafeBytes { (buffer: UnsafePointer<UInt8>) -> String? in
      let ptr = UnsafeMutablePointer<UInt8>.allocate(capacity: count * 2)
      for i in 0 ..< count {
        ptr[i*2] = itoh((buffer[i] >> 4) & 0xF)
        ptr[i*2+1] = itoh(buffer[i] & 0xF)
      }
      return String(bytesNoCopy: ptr, length: count*2, encoding: String.Encoding.utf8, freeWhenDone: true)
    }
  }
}
